<?php
/**
 * @file
 * A basic  Migrate module to import nodes from mongo database.
 */

/**
 * You must implement hook_migrate_api().
 */
function migrate_mongo_migrate_api() {

  $api = array(
    'api' => 2,
    'groups' => array(
      'city' => array(
        'title' => t('City'),
      ),
    ),

    'migrations' => array(
      'CityNode' => array(
        'class_name' => 'CityNodeMigration',
        'group_name' => 'city',
      ),
    ),
  );
  return $api;
}

/**
 * A migration from mongo db.
 * and creates Drupal nodes of type 'city'
 */
class CityNodeMigration extends Migration {
  /**
   * A Constructor.
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Set source.
    $db_string = 'mongodb://jitu:1234@localhost:27017/city';
    $m = new MongoClient();
    $db = $m->city;
    $collection = $db->cities;
    $cursor = $collection->find();

    $fields = array(
      '_id' => '_id',
    );

    $query = array();

    $sort = array(
      '_id' => 1,
    );

    $this->source = new MigrateSourceMongoDB($collection, $query, $fields, $sort);

    // Set destination.
    $this->destination = new MigrateDestinationNode('city');

    // Set map.
    $source_key = array(
      '_id' => array(
        'type' => 'varchar',
        'length' => 24,
        'not null' => TRUE,
        'description' => 'MongoDB ID field.',
      ),
      'city' => array(
        'type' => 'varchar',
        'length' => 24,
        'not null' => TRUE,
        'description' => 'MongoDB city field.',
      ),
      'loc' => array(
        'type' => 'varchar',
        'length' => 24,
        'not null' => TRUE,
        'description' => 'MongoDB loc field.',
      ),
      'pop' => array(
        'type' => 'varchar',
        'length' => 24,
        'not null' => TRUE,
        'description' => 'MongoDB pop field.',
      ),
      'state' => array(
        'type' => 'varchar',
        'length' => 24,
        'not null' => TRUE,
        'description' => 'MongoDB state field.',
      ),

    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationNode::getKeySchema());

    // Mapped fields.
    $this->addFieldMapping('title', '_id')
         ->description(t('Mapping id'));

    $this->addFieldMapping('field_city', 'city')
         ->description(t('Mapping city'));

    $this->addFieldMapping('field_loc', 'loc')
         ->description(t('Mapping loc'));

    $this->addFieldMapping('field_pop', 'pop')
         ->description(t('Mapping pop'));

    $this->addFieldMapping('field_state', 'state')
         ->description(t('Mapping state'));

    $this->addUnmigratedDestinations(array(
      'changed',
      'comment',
      'created',
      'is_new',
      'language',
      'log',
      'translate',
      'promote',
      'revision',
      'revision_uid',
      'status',
      'tnid',
      'path',
      'sticky',
      'uid',
    ));

  }
}
